/**
 * The package containing all components that are found thanks to the Service Provider Interfaces
 * technology.
 *
 * @author Nicolas Filotto (nicolas.filotto@gmail.com)
 * @version $Id$
 * @since 1.0
 */
package com.gitlab.essobedo.appma.spi;
