/**
 * The package containing everything related to the internationalization of the application.
 *
 * @author <a href="mailto:nicolas.filotto@gmail.com">Nicolas Filotto</a>
 * @version $Id$
 */
package com.gitlab.essobedo.appma.i18n;
