/**
 * The package containing all the classes allowing to be notified of any progress of a given task.
 *
 * @author Nicolas Filotto (nicolas.filotto@gmail.com)
 * @version $Id$
 * @since 1.0
 */
package com.gitlab.essobedo.appma.core.progress;
