/**
 * The package containing all the main classes of the application manger.
 *
 * @author Nicolas Filotto (nicolas.filotto@gmail.com)
 * @version $Id$
 * @since 1.0
 */
package com.gitlab.essobedo.appma.core;
