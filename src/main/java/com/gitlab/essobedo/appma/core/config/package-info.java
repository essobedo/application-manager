/**
 * The package containing all the classes allowing to manipulate the {@code Configuration}.
 *
 * @author Nicolas Filotto (nicolas.filotto@gmail.com)
 * @version $Id$
 * @since 1.0
 */
package com.gitlab.essobedo.appma.core.config;
